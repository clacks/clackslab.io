---

Clacks

---

**Contents**  

- [Clacks](#clacks)
- [Attributions](#attributions)
- [Usage](#usage)
- [License](#license)


## Clacks

The clacks in Terry Pratchett's Discworld novels is a network of semaphore towers stretching along the Sto Plains, into the Ramtops and across the Unnamed Continent to Genua. It was introduced in The Fifth Elephant and has become the Discworld's first telecommunications network. While the system structure is that of a telegraph, elements of it are often described as similar to the Internet.

The history of the clacks network was detailed in Going Postal. The invention was originally made by an artificer called Robert Dearheart who was conducting experiments in an abandoned wizard's tower halfway between Ankh-Morpork and Sto Lat, where he came up with the basic mechanism of two-by-three array of wooden panels, with pulleys that could drop shutters over them, creating a code. A series of high towers, with one of these mechanisms on each side and someone ready to relay the codes, could send messages across "at the speed of light". The panel also had a recess for a lamp, meaning messages could be sent at night. Based on this, he founded the Grand Trunk Company which began creating a network of towers that would stretch across the continent.

[Clacks game], [gnuterrypratchett.com/]

## Attributions

- Icons: [chrome-clacks]

- Backspindle Games


## Usage

Simple web page, ussing browserify and bootstrap.

PDF creation made on client side with [html2pdf]

```
npm install // install modules
npm run start // create a beefy dev server
npm run watch // live reload
```

[Clacks message example]

![GNUterryClacks.png](https://clacks.gitlab.io/dist/images/GNUterryClacks.png)

## License

[GNU General Public License version 3]

[chrome-clacks]: https://github.com/newfolder0/chrome-clacks
[Clacks Game]: https://boardgamegeek.com/boardgame/140279/clacks-discworld-board-game
[gnuterrypratchett.com/]: http://www.gnuterrypratchett.com/
[html2pdf]: https://ekoopmans.github.io/html2pdf.js/
[GNU General Public License version 3]: https://opensource.org/licenses/GPL-3.0
[Clacks message example]: https://clacks.gitlab.io/dist/images/clacks_message.pdf
