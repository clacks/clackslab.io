/* global event */
var $ = require('jquery')

require('@popperjs/core/dist/umd/popper')
require('bootstrap/dist/js/bootstrap')

var cssify = require('cssify')
cssify.byUrl('./dist/css/bootstrap.min.css')
console.log('GNU Terry Pratchett')

var Clacks = require('./create_clacks')
$('#createPDF').hide()
$('#mensaje').submit(function () {
  var msg = $('input').first().val()
  Clacks.addText(msg)
  event.preventDefault()
})

$('#createPDF').submit(function () {
  Clacks.createClacksPDF()
  event.preventDefault()
})
