/* global html2pdf */

// Convert plain text to clacks:
var _ = require('lodash')
var $ = require('jquery')

var Clacks = {
  // prepare text
  addText: function (text) {
    console.log('texto original: ', text)
    var kebabMessage = _.kebabCase(text)
    var message = _.toUpper(kebabMessage)
    console.log('clacks text: ', message)
    this.createClacks(message)
  },
  // convert letters to clack images:
  createClacks: function (message) {
    var clacksArray = []
    _.forEach(message, function (value) {
      if (value === '-') {
        clacksArray.push([value, 'Clacks-SPACE.png'])
      } else {
        clacksArray.push([value, 'Clacks-' + value + '.png'])
      }
    })
    // add end signal:
    clacksArray.push(['end', 'Clacks-END.png'])
    this.createClacksHTML(clacksArray)
  },
  // create html with clack message above
  createClacksHTML: function (clacksArray) {
    var originalText = ''
    var clacksMesage = ''
    var imageFolder = './dist/images/backspindle_icons/Clacks38/'
    _.forEach(clacksArray, function (value) {
      var clackImage = '<img src="' + imageFolder + value[1] + '" class="img-rounded" alt="' + value[1] + '">'
      originalText = originalText + '<th>' + value[0] + '</th>'
      clacksMesage = clacksMesage + '<td>' + clackImage + '</td>'
    })
    var tableHead = '<thead><tr>' + originalText + '</tr></thead>'
    var tableBody = '<tbody><tr>' + clacksMesage + '</tr></tbody>'
    $('#clacksTable').append(tableHead)
    $('#clacksTable').append(tableBody)
    $('#pdfbtn').show()
  },
  createClacksPDF: function () {
    var opt = {
      margin: 0.1,
      filename: 'clacks.pdf',
      image: { type: 'jpeg', quality: 0.98 },
      html2canvas: { scale: 5 },
      jsPDF: { unit: 'in', format: 'a4', orientation: 'landscape' }
    }
    var HTML = document.body
    $('#navbarSupportedContent').remove()
    $('#createPDF').remove()
    $('.table-responsive').prepend('<p style="font-family: Courier New;">MESSAGE:</p>')
    var clacksHeaderHTM = '<table style="font-family: Courier New;" class="table table-borderless">' +
                          '<thead><tr><th>Form No. 382</th><th></th><th></th><th>Ankh-Morpork Branch</th></tr></thead>' +
                          '<tbody><tr><td >CLACKS TRANSMISSION REQUEST FORM</td></tr></tbody></table>'
    $('.container-fluid').append(clacksHeaderHTM)
    var tableCaption = '<caption><p>Made with © <a href="https://clacks.gitlab.io/">https://clacks.gitlab.io/</a></p></caption>'
    $('#clacksTable').append(tableCaption)
    // Create pdf
    html2pdf().set(opt).from(HTML).toPdf().save()
  }
}

module.exports = Clacks
